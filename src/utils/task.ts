import { useHttp } from "utils/http"
import { QueryKey, useMutation, useQuery } from "react-query"
import { Task } from "types/task"
import { useDeleteConfig, useReorderKanbanConfig } from "./use-optimistic-options"
import { SortProps } from "./kanban"

export const useTasks = (param?: Partial<Task>) => {
  const client = useHttp()
  return useQuery<Task[]>(['tasks',param],()=>client('tasks',{data:param}))
}

export const useDeleteTask = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation(({id}:{id:number})=>
    client(`tasks/${id}`,{
      method: 'DELETE'
    }),
    useDeleteConfig(queryKey)
  )
}

export const useRecorderTask = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation(
    (params:SortProps)=>{
      return client("tasks/reorder",{
        data:params,
        method:'POST'
      })
    },
    useReorderKanbanConfig(queryKey)
  )
}