import { useHttp } from "utils/http"
import { QueryKey, useMutation, useQuery } from "react-query"
import { Kanban } from "types/kanban"
import { useAddConfig, useDeleteConfig, useEditConfig, useReorderKanbanConfig } from "./use-optimistic-options"
import { Task } from "types/task"

export const useKanbans = (param?: Partial<Kanban>) => {
  const client = useHttp()
  return useQuery<Kanban[]>(['kanbans',param],()=>client('kanbans',{data:param}))
}

export const useAddKanban = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation((params:Partial<Kanban>)=>
    client(`kanbans`,{
      data:params,
      method: 'POST'
    }),
    useAddConfig(queryKey)
  )
}

export const useAddTask = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation((params:Partial<Task>)=>
    client(`tasks`,{
      data:params,
      method: 'POST'
    }),
    useAddConfig(queryKey)
  )
}

export const useTask = (id?:number)=>{
  const client = useHttp()
  return useQuery<Task>(
    ["task",{id}],
    ()=>client(`tasks/${id}`),
    {
      enabled:!!id,
    }
  )
}

export const useEditTask = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation(
    (params:Partial<Task>)=>
      client(`tasks/${params.id}`,{
        data:params,
        method: 'PATCH'
      }),
      useEditConfig(queryKey)
  )
}

export const useDeleteKanban = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation(({id}:{id:number})=>
    client(`kanbans/${id}`,{
      method: 'DELETE'
    }),
    useDeleteConfig(queryKey)
  )
}

export interface SortProps{
  fromId:number,
  type:'before'|'after',
  referenceId:number,
  fromKanbanId?: number;
  toKanbanId?: number;
}

export const useRecorderKanban = (queryKey:QueryKey)=>{
  const client = useHttp()

  return useMutation(
    (params:SortProps)=>{
      return client("kanbans/reorder",{
        data:params,
        method:'POST'
      })
    },
    useReorderKanbanConfig(queryKey) 
  )
}
