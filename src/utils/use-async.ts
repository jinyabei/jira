import { useState } from "react"
import { useMountedRef } from "utils"

interface State<D>{
  error:Error|null
  data:D|null,
  stat:'idle'|'loading'|'error'|'success'
}

const defaultInitialState:State<null> = {
  stat:'idle',
  data:null,
  error:null
}

const defaultConfig = {
  throwOnError:false,
}

export const useAsync = <D>(initialState?:State<D>,inititalConfig?:typeof defaultConfig)=>{
  const config = {...defaultConfig,...inititalConfig};

  const [state,setState] = useState<State<D>>({
    ...defaultInitialState,
    ...initialState
  })

  const [retry,setRetry] = useState(()=>()=>{})

  const mountedRef = useMountedRef()

  const setData = (data:D)=>setState({
    stat:'success',
    data,
    error:null
  })

  const setError = (error:Error)=>setState({
    stat:'error',
    data:null,
    error
  })

  const run = (
    promise:Promise<D>,
    runConfig?:{retry:()=>Promise<D>}
  )=>{
    if(!promise||!promise.then){
      throw new Error("请传入Promise类型的数据")
    }
    setRetry(()=>()=>{
      if(runConfig?.retry){
        run(runConfig?.retry(),runConfig)
      }
    })
    setState({...state,stat:'loading'})
    return promise.then(data=>{
      //在已挂载的组件，才进行赋值
      if(mountedRef.current){
        setData(data)
      }
      return data
    }).catch(error=>{
      setError(error)
      if(config.throwOnError){
        return Promise.reject(error)
      }else{
        return error
      }
    })
  }

  return {
    isIdle: state.stat === "idle",
    isLoading: state.stat === "loading",
    isError: state.stat === "error",
    isSuccess: state.stat === "success",
    run,
    setData,
    setError,
    retry,
    ...state,
  }
}