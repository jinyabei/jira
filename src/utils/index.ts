import { useEffect, useRef, useState } from "react"

export const isFasly = (value:unknown)=>value===0?false:!value

export const isVoid = (value:unknown)=>value===null||value===undefined||value===''

export const cleanObject = (object:{[key:string]:unknown})=>{
  const result = {...object}
  Object.keys(result).forEach(key=>{
    const value = result[key] 
    if(isVoid(value)){
      delete result[key]
    }
  })
  return result
}

export const useMount = (callback:()=>void)=>{
  useEffect(()=>{
    callback()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
}

export const useDebounce =<V>(value:V,delay?:number)=>{
  const [debouncedValue,setDebouncedValue] = useState(value)
  useEffect(()=>{
    const timeout = setTimeout(()=>setDebouncedValue(value),delay)
    return ()=>clearTimeout(timeout)
  },[value,delay])
  return debouncedValue
}

export const useDocumentTitle = (title:string,keepOnmount:boolean=true)=>{
  const oldTitle = useRef(document.title).current

  useEffect(()=>{
    document.title = title
  },[title])

  useEffect(()=>{
    return ()=>{
      if(!keepOnmount){
        document.title = oldTitle
      }
    }
  },[keepOnmount,oldTitle])
}

export const resetRoute = ()=>window.location.href = window.location.origin

/**
 * 返回组件的挂载状态,如果还没挂载或者已经卸载,返回false,反之,返回true
 */
export const useMountedRef = ()=>{
  const mountedRef = useRef(false)

  useEffect(()=>{
    mountedRef.current = true
    return ()=>{
      mountedRef.current = false
    }
  })

  return mountedRef
}