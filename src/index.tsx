import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {DevTools,loadServer} from 'jira-dev-tool';
import 'antd/dist/antd.less'
import { AppProviders } from 'context';
import './wdyr'
import { Profiler } from 'components/profiler';

loadServer(()=>{
  ReactDOM.render(
    <React.StrictMode>
      <Profiler id={"Root App"} phases={["mount"]}>
        <AppProviders>
          <DevTools />
          <App />
        </AppProviders>
      </Profiler>
    </React.StrictMode>,
    document.getElementById('root') 
  )
})

reportWebVitals();
