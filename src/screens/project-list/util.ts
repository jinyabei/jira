import { useMemo, useState } from "react";
import { useQueryParam, useSetUrlSearchParam } from "utils/url";
import { useProject } from "../../utils/project";

export const useProjectsSearchParams = ()=>{
  const [keys] = useState<('name'|'personId')[]>(["name","personId"])
  const [param,setParam] = useQueryParam(keys)
  return [useMemo(()=>({...param,personId:Number(param.personId)||undefined}),[param]),setParam] as const
}

export const useProjectModal = ()=>{
  const [{projectCreate},setProjectCreate] = useQueryParam(['projectCreate'])
  const [{editingProjectId},setEditingProjectId] = useQueryParam(["editingProjectId"])
  const {data:editingProject,isLoading} = useProject(Number(editingProjectId))

  const setUrlParams = useSetUrlSearchParam()

  const open = ()=>setProjectCreate({projectCreate:true})
  const close = ()=>{
    setUrlParams({projectCreate:'',editingProjectId:''})
  }
  const startEdit = (id:number)=>setEditingProjectId({editingProjectId:id})

  return {
    projectModalOpen:projectCreate==='true'||Boolean(editingProjectId),
    open,
    close,
    startEdit,
    editingProject,
    isLoading
   } as const
}

export const useProjectsQueryKey = ()=>{
  const [params] = useProjectsSearchParams()
  return ["projects",params]
}