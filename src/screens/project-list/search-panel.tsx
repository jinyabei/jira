import { Input, Form } from "antd"
import { UserSelect } from "components/user-select"
import { Project } from "types/project"
import { User } from "types/user"
//注意不写这行，css不生效
/** @jsxImportSource @emotion/react */

interface SearchPanelProps{
  users:User[],
  param:Partial<Pick<Project,'name'|'personId'>>
  setParam:(param:SearchPanelProps["param"])=>void
}
export const  SearchPanel = ({param,setParam,users}:SearchPanelProps)=>{
  return  (
    <Form layout={'inline'} css={{marginBottom:'2rem'}}>
      <Form.Item>
        <Input type="text" 
          placeholder="项目名"
          value={param.name} onChange={evt=>
          setParam({
            ...param,
            name:evt.target.value
          })}/>
        </Form.Item>
        <Form.Item>
          <UserSelect 
            defaultOptionName={'负责人'}
            value={param.personId} 
            onChange = {
            (value)=>setParam({
              ...param,
              personId:value
            })
          } />
      </Form.Item>
    </Form>
  )
}
