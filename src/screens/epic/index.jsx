import { Row, ScreenContainer } from "components/lib"
import { useDeleteEpic, useEpics } from "utils/epic"
import { List, Modal } from "antd"
import { Button } from "antd/lib/radio"
import dayjs from "dayjs"
import { useTasks } from "utils/task"
import { Link } from "react-router-dom"
import { useEpicSearchParams, useEpicsQueryKey } from "./util"
import { useProjectInUrl } from "screens/kanban/util"
import { Epic } from "types/epic"
import { CreateEpic } from "./create-epic"
import { useState } from "react"

export const EpicScreen = ()=>{
  const {data:currentProject} = useProjectInUrl()
  const {data:epics} = useEpics(useEpicSearchParams())
  const {data:tasks} = useTasks({projectId:currentProject?.id})
  const {mutate:deleteEpic} = useDeleteEpic(useEpicsQueryKey())
  const [epicCreateOpen,setEpicCreateOpen] = useState(false)

  const ConfirmDeleteEpic = (epic: Epic)=>{
    Modal.confirm({
      title:'确定删除看板',
      okText:'确定',
      cancelText:'取消',
      onOk:()=>{
        deleteEpic({id:epic.id})
      }
    })
  }

  return (
    <ScreenContainer>
      <Row between={true}>
        <h1>{currentProject?.name}任务组</h1>
        <Button type={'link'} onClick={()=>setEpicCreateOpen(true)}>创建任务组</Button>
      </Row>
      <List dataSource={epics} itemLayout="vertical" style={{overflowY:'scroll'}}
        renderItem={epic=>
          <List.Item>
            <List.Item.Meta 
              title={
                <Row between={true}>
                  <span>{epic.name}</span>
                  <Button type={"link"} onClick={()=>ConfirmDeleteEpic(epic)}>删除</Button>
                </Row>
              }
              description={
                <div>
                  <div>开始时间:{dayjs(epic.start).format("YYYY-MM-DD")}</div>
                  <div>结束时间:{dayjs(epic.end).format("YYYY-MM-DD")}</div>
                </div>
              }
            >
            </List.Item.Meta>
            <div>
              {tasks?.filter(task=>task.epicId===epic.id).map(task=>(
                <Link key={task.id} to={`/projects/${currentProject?.id}/kanban?editingTaskId=${task.id}`}>
                  {task.name}
                </Link>
              ))}
            </div>
          </List.Item>
        }
      >
      </List>
      <CreateEpic onClose={()=>setEpicCreateOpen(false)} visible={epicCreateOpen}/>
    </ScreenContainer>
  )
}