import { useForm } from "antd/lib/form/Form"
import { useTaskModal, useTasksQueryKey } from "./util"
import { useEditTask } from "utils/kanban"
import { useEffect } from "react"
import { Button, Form, Input, Modal } from "antd"
import { UserSelect } from "components/user-select"
import { TaskTypeSelect } from "./task-type-select"
import { useDeleteTask } from "utils/task"

const layout  = {
  labelCol:{span:8},
  wrapperCol:{span:16}
}

export const TaskModal = ()=>{
  const [form] = useForm()
  const {editingTaskId,editingTask,close} = useTaskModal()
  const {mutateAsync:editTask,isLoading:editLoading} = useEditTask(useTasksQueryKey())
  const {mutate:deleteTask} = useDeleteTask(useTasksQueryKey())

  const onCancel = ()=>{
    close()
    form.resetFields()
  }

  const onOK = async()=>{
    await editTask({...editingTask,...form.getFieldsValue()})
    close()
  }

  const startDelete = ()=>{
    close()
    Modal.confirm({
      title:'确定删除任务',
      okText:'确定',
      cancelText:'取消',
      onOk:()=>{
        return deleteTask({id:Number(editingTaskId)})
      }
    })
  }

  useEffect(()=>{
    form.setFieldsValue(editingTask)
  },[form,editingTask])

  return  (
    <Modal 
      forceRender={true}
      onCancel={onCancel}
      onOk={onOK}
      okText={'确认'} 
      cancelText={'取消'}
      confirmLoading={editLoading}
      title={"编辑任务"}
      open={!!editingTaskId}
    >
      <Form initialValues={editingTask} form={form} {...layout}>
        <Form.Item label="任务名" name={'name'} 
          rules={[{required:true,message:'请输入任务名'}]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="经办人" name={'processorId'} 
        >
          <UserSelect defaultOptionName="经办人" />
        </Form.Item>
        <Form.Item label="类型" name={'typeId'} 
          rules={[{required:true,message:'请输入任务名'}]}
        >
          <TaskTypeSelect />
        </Form.Item>
      </Form>
      <div style={{textAlign:'right'}}>
        <Button style={{fontSize:'14px'}} size={"small"} onClick={startDelete}>删除</Button>
      </div>
    </Modal>
  )
}