import { useCallback, useMemo } from "react"
import { useLocation } from "react-router"
import { useDebounce } from "utils"
import { useTask } from "utils/kanban"
import { useProject } from "utils/project"
import { useQueryParam } from "utils/url"

export const useProjectIdInUrl = ()=>{
  const {pathname} = useLocation()
  const id = pathname.match(/projects\/(\d+)/)?.[1]
  return Number(id)
}

export const useProjectInUrl = ()=>useProject(useProjectIdInUrl())

export const useKanbanSearchParams = ()=>({projectId:useProjectIdInUrl()})

export const useKanbansQueryKey = ()=>['kanbans',useKanbanSearchParams()]

export const useTasksSearchParams = ()=>{
  //获取参数
  const [param,setParam] = useQueryParam([
    "name",
    "typeId",
    "processorId",
    "tagId"
  ])
  const projectId = useProjectIdInUrl()

  const debouncedName = useDebounce(param.name ,200)

  //缓存参数
  return useMemo(()=>({
    projectId,
    typeId:Number(param.typeId)||undefined,
    processorId:Number(param.processorId)||undefined,
    tagId:Number(param.tagId)||undefined,
    name:debouncedName
  }),[projectId,param])
}

export const useTasksQueryKey = ()=>['tasks',useTasksSearchParams()]

export const useTaskModal = ()=>{
  const [{editingTaskId},setEditingTaskId] = useQueryParam(['editingTaskId'])
  const {data:editingTask,isLoading} = useTask(Number(editingTaskId))
  const startEdit = useCallback((id:number)=>{
    setEditingTaskId({editingTaskId:id})
  },[setEditingTaskId])

  const close = useCallback(()=>{
    setEditingTaskId({editingTaskId:''})
  },[setEditingTaskId])

  return {
    editingTaskId,
    editingTask,
    startEdit,
    close,
    isLoading
  }
}