export default { 
  extends: ['@commitlint/config-conventional'],
  rules: {
    // 自定义规则，例如提交类型的枚举
    'type-enum': [
      2,
      'always',
      [
        'feat', // 新功能
        'fix', // 修复缺陷
        'docs', // 文档变更
        'tyle', // 代码格式调整
        'efactor', // 代码重构
        'perf', // 性能优化
        'test', // 测试用例修改
        'build', // 构建相关修改
        'ci', // CI配置修改
        'evert', // 回滚提交
        'chore', // 其他杂项修改
      ],
    ],
  }, 
};
